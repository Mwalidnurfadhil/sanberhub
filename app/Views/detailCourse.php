<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="mr-3">
                <h1 class="m-0">Course</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb left">
                    <li class="breadcrumb-item">Content</li>
                    <li class="breadcrumb-item active">Course</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <!-- Page Heading -->
                        <h3 class="h3 mb-4 text-gray-800">Course Detail</h3>

                    </div>
                    <div class="card-body">
                        <div class="col-md-12 mt-3">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <img src="/img/<?= $course[0]['thumbnails'] ?>" alt="" width="500">
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-group">
                                        <p class="mb-0">Title</p>
                                        <?= $course[0]['title'] ?>
                                    </div>
                                    <div class="form-group">

                                        <p class="mb-0">Category</p>
                                        <?= $course[0]['category'] ?>
                                    </div>

                                    <div class="form-group">
                                        <p class="mb-0">Competencies</p>
                                        <?php

                                        foreach ($course as $co) :
                                        ?>
                                            <?= $co['namaCompetencies'] ?>
                                        <?php endforeach; ?>
                                    </div>
                                    <div class="form-group">
                                        <p class="mb-0">Caption</p>
                                        <?= $course[0]['caption'] ?>
                                    </div>
                                    <div class="form-group">
                                        <p for="organization" class="mb-0">Updated By</p>
                                        <p>Admin</p>

                                    </div>

                                </div>
                            </div>

                        </div>
                        <div class="col-md-12 mt-2">
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <p class="mb-0">Created at</p>
                                    <?= $course[0]['created_at'] ?>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-0">Last Update</p>
                                    <?= $course[0]['updated_at'] ?>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <p class="mb-0">Status</p>
                                    <?php if ($course[0]['status'] = 'publish') { ?>
                                        <div class="badge badge-pill badge-primary">
                                            <?= $course[0]['status'] ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="badge badge-pill badge-danger">
                                            <?= $course[0]['status'] ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-6">
                                    <p class="mb-0">Privacy</p>
                                    <?= $course[0]['privacy'] ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 mt-2">

                            <p>Content</p>
                            <hr>
                        </div>
                    </div>
                    <div class="card-footer">
                        <h3 class="card-title col-md-10">.</h3>
                        <div class="card">
                            <a class=" btn btn-outline-danger" href="/course/addCourse">Publish Course</a>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>

<?= $this->endSection(); ?>