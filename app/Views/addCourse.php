<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="mr-3">
                <h1 class="m-0">Course</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb left">
                    <li class="breadcrumb-item">Content</li>
                    <li class="breadcrumb-item active">Course</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
<div class="content">


    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <!-- Page Heading -->
                        <h3 class="h3 mb-4 text-gray-800">Add Course</h3>

                    </div>
                    <div class="card-body">

                        <form class="mb-5" action="/course/saveCourse" method="POST" enctype="multipart/form-data">

                            <label class="ml-2 mt-3 mb-0">Course Details</label>
                            <hr>
                            <div class="col-md-12 mt-2">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <p for="title" class="mb-0">Title
                                            <span class="form-required"> * </span>
                                        </p>
                                        <input type="text" class="form-control" id="title" name="title" required>
                                    </div>
                                    <div class="col-md-6">
                                        <p for="thumbnails" class="mb-0">Thumbnails</p>

                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="thumbnails" name="thumbnails">
                                            <label class="custom-file-label" for="thumbnails">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <p for="starDate" class="mb-0">Star Date
                                            <span class="form-required"> * </span>
                                        </p>
                                        <input type="date" class="form-control " id="starDate" name="starDate">
                                    </div>
                                    <div class="col-md-6">
                                        <p for="endDate" class="mb-0">End Date
                                            <span class="form-required"> * </span>
                                        </p>
                                        <input type="date" class="form-control " id="endDate" name="endDate">

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <p for="caption" class="mb-0">Caption</p>
                                        <textarea type="text" class="form-control" id="caption" name="caption"></textarea>
                                    </div>

                                    <div class="col-md-6">
                                        <p for="description" class="mb-0">Description</p>
                                        <textarea type="text" class="form-control " id="description" name="description"></textarea>

                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-6">
                                    <label class="ml-2 mt-3 mb-0">Content Category</label>
                                    <hr>
                                </div>
                                <div class="col-md-6">
                                    <label class="ml-2 mt-3 mb-0">Content Control</label>
                                    <hr>
                                </div>
                            </div>

                            <div class="col-md-12 mt-3">
                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p for="level" class="mb-0">Level
                                                <span class="form-required"> * </span>
                                            </p>
                                            <input type="text" class="form-control" id="level" name="level" required>
                                        </div>

                                        <div class="form-group">
                                            <p for="category" class="mb-0">Category
                                                <span class="form-required"> * </span>
                                            </p>
                                            <select class="custom-select mr-sm-2" id="category" name="category" required>
                                                <option selected>--Pilih--</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <p for="competencies" class="mb-0">Competencies Based Category
                                                <span class="form-required"> * </span>
                                            </p>
                                            <select class="select2" multiple="multiple" id=namaCompetencies[] name=namaCompetencies[] data-placeholder="Select a State" style="width: 100%;" required>
                                                <option>Lorem</option>
                                                <option>Lorem</option>
                                                <option>California</option>
                                                <option>Delaware</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <p for="businessDomain" class="mb-0">Business Domain
                                                <span class="form-required"> * </span>
                                            </p>
                                            <select class="select2" multiple="multiple" id="namaBusinessDomain[]" name="namaBusinessDomain[]" data-placeholder="Select a State" style="width: 100%;" required>
                                                <option>Lorem</option>
                                                <option>Lorem</option>
                                                <option>California</option>
                                                <option>Delaware</option>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <p>Privacy</p>
                                            <input type="radio" name="privacy" value="public"> <label for="">Public</label> (Everyone can watch your video) <br>
                                            <input type="radio" name="privacy" value="protected"> <label for="">Protected</label>(Everyone wiht link can watch your video)<br>
                                            <input type="radio" name="privacy" value="private"> <label for="">Private</label> (Only someone with access can watch your video)
                                        </div>
                                        <div class="form-group">

                                            <p>Sequence</p>
                                            <input type="radio" name="sequence" value="public"> <label for="">Random</label>(User can select content without paying attention to the order of the content. (Course Flow) )<br>
                                            <input type="radio" name="sequence" value="protected"> <label for="">Sequence</label> (User are required to follow the course flow.)<br>
                                        </div>

                                        <div class="form-group">
                                            <p>Certificate</p>
                                            <input type="radio" name="certified" value="protected"> <label for="">Certificate</label> (Everyone can watch your video)<br>
                                            <input type="radio" name="certified" value="public"> <label for="">No Certificate</label> ( Everyone wiht link can watch your video ) <br>
                                        </div>
                                        <div class="form-group">
                                            <p for="status" class="mb-0">Status
                                                <span class="form-required"> * </span>
                                            </p>
                                            <select class="custom-select mr-sm-2" id="status" name="status">
                                                <option selected>Status</option>
                                                <option value="Publish">Publish</option>
                                                <option value="Draft">Draft</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <p for="organization" class="mb-0">Organization
                                                <span class="form-required"> * </span>
                                            </p>
                                            <select class="select2" multiple="multiple" id="namaOrganization[]" name="namaOrganization[]" data-placeholder="Select a State" style="width: 100%;">
                                                <option>Lorem</option>
                                                <option>Lorem</option>
                                                <option>California</option>
                                                <option>Delaware</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <p for="learningHourse" class="mb-0">Learning Hourse
                                                <span class="form-required"> * </span>
                                            </p>
                                            <select class="custom-select mr-sm-2" id="learningHours" name="learningHours" data-placeholder="Select a State" style="width: 100%;">
                                                <option>Lorem</option>
                                                <option>Lorem</option>
                                                <option>California</option>
                                                <option>Delaware</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn btn-danger">Add</button>
                                <a href="/course" type="button" class="btn btn-outline-danger">Discard</a>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection(); ?>