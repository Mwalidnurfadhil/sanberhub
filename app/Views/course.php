<?= $this->extend('templates/index'); ?>
<?= $this->section('page-content'); ?>

<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="mr-3">
                <h1 class="m-0">Course</h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb left">
                    <li class="breadcrumb-item">Content</li>
                    <li class="breadcrumb-item active">Course</li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">


                        <h3 class="card-title col-md-10">Course List</h3>
                        <div class="card">
                            <a class=" btn btn-outline-danger" href="/course/addCourse"><i class="fas fa-plus fa-sn"></i> Add Course</a>
                        </div>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>NO</th>
                                    <th>TITLE</th>
                                    <th>ORGANIZATION</th>
                                    <th>STATUS</th>
                                    <th>LAST UPDATE</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($course as $co) :
                                ?>
                                    <tr>
                                        <td><?= $no++ ?></td>
                                        <td><?= $co['title'] ?></td>
                                        <td><?= $co['namaOrganization'] ?></td>
                                        <td>
                                            <?php if ($co['status'] = 'publish') { ?>
                                                <div class="badge badge-pill badge-primary">
                                                    <?= $co['status'] ?>
                                                </div>
                                            <?php } else { ?>
                                                <div class="badge badge-pill badge-danger">
                                                    <?= $co['status'] ?>
                                                </div>
                                            <?php } ?>
                                        </td>
                                        <td><?= date('d-m-yy', strtotime($co['updated_at'])) ?>

                                        </td>
                                        <td style="text-align:center">
                                            <a href="/course/<?= $co['idCourse'] ?>" class="btn btn-info btn-sm"><i class="fas fa-search-plus"> Detail</i></a>
                                            <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#hapusCourse"><i class="fas fa-trash"> Hapus</i></button>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>

                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->

    </div>
    <div class="modal fade" id="hapusCourse" tabindex="-1" aria-labelledby="exampleModalhapusCourse" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/course/<?= $co['idCourse'] ?>" method="POST" class="d-inline">
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-xs-8">
                                <label class="control-label col-xs-3">Are you sure?</label>
                                <p>You want to delete this Course</p>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger">Yes, delete it!</i></button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- /.content -->

<?= $this->endSection(); ?>