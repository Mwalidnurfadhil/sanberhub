<?php

namespace App\Models;

use CodeIgniter\Model;

class OrganizationModel extends Model
{
    protected $table = 'organization';
    protected $primaryKey = 'idOrganization';

    protected $allowedFields = [
        'idOrganization', 'idCourse', 'namaOrganization'
    ];
}
