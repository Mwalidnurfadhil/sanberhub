<?php

namespace App\Models;

use CodeIgniter\Model;

class CompetenciesModel extends Model
{
    protected $table = 'competencies';
    protected $primaryKey = 'idCompetencies';

    protected $allowedFields = [
        'idCompetencies', 'idCourse', 'namaCompetencies'
    ];
}
