<?php

namespace App\Models;

use CodeIgniter\Model;

class BusinessDomainModel extends Model
{
    protected $table = 'businessdomain';
    protected $primaryKey = 'idBusinessDomain';

    protected $allowedFields = [
        "idBusinessDomain", "idCourse", "namaBusinessDomain"
    ];
}
