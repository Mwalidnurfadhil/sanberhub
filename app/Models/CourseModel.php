<?php

namespace App\Models;

use CodeIgniter\Model;

class CourseModel extends Model
{
    protected $table = 'course';
    protected $primaryKey = 'idCourse';
    protected $useTimestamps = true;

    protected $allowedFields = [
        'idCourse', 'title', 'thumbnails', 'starDate', 'endDate', 'caption', 'description', 'level', 'category', 'privacy', 'sequence', 'certified', 'status', 'learningHours'
    ];

    public function getCourse()
    {
        return $this->db->table('course')
            ->join('organization', 'organization.idCourse=course.idCourse')
            ->get()->getResultArray();
    }

    public function getCourseDetail($idCourse)
    {
        return $this->db->table('course')
            ->join('organization', 'organization.idCourse=course.idCourse')
            ->join('competencies', 'competencies.idCourse=course.idCourse')
            ->join('businessdomain', 'businessdomain.idCourse=course.idCourse')
            ->where('course.idCourse', $idCourse)
            ->get()->getResultArray();
    }
}
