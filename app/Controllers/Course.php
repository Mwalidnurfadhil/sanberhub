<?php

namespace App\Controllers;

use App\Models\BusinessDomainModel;
use App\Models\CompetenciesModel;
use App\Models\CourseModel;
use App\Models\OrganizationModel;

class Course extends BaseController
{
    protected $courseModel;
    protected $businessDomainModel;
    protected $competenciesModel;
    protected $organizationModel;

    public function __construct()
    {
        $this->courseModel = new CourseModel();
        $this->businessDomainModel = new BusinessDomainModel();
        $this->competenciesModel = new CompetenciesModel();
        $this->organizationModel = new OrganizationModel();
    }
    public function index()
    {
        $data = [
            'course' => $this->courseModel->getCourse()
        ];
        return view('course', $data);
    }

    public function addCourse()
    {
        return view('addCourse');
    }

    public function saveCourse()
    {
        // dd($this->request->getVar());
        //ambil gambar
        $fileThumbnails = $this->request->getFile('thumbnails');
        // dd($fileThumbnails);

        //pindah file ke folder img
        $fileThumbnails->move('img');
        //ambil nama file
        $namaThumbnails = $fileThumbnails->getName();

        // insert ke tabel atlet
        $data = [
            'title' => $this->request->getVar('title'),
            'thumbnails' => $namaThumbnails,
            'starDate' => $this->request->getVar('starDate'),
            'endDate' => $this->request->getVar('endDate'),
            'caption' => $this->request->getVar('caption'),
            'description' => $this->request->getVar('description'),
            'level' => $this->request->getVar('level'),
            'category' => $this->request->getVar('category'),
            'privacy' => $this->request->getVar('privacy'),
            'sequence' => $this->request->getVar('sequence'),
            'certified' => $this->request->getVar('certified'),
            'status' => $this->request->getVar('status'),
            'learningHours' => $this->request->getVar('learningHours')
        ];

        $this->courseModel->insert($data);

        $idCourse = $this->courseModel->getInsertID();

        $competencies = $this->request->getVar('namaCompetencies');
        $jum = -1;

        foreach ($competencies as $comp) {
            $jum++;
            $result = [
                'namaCompetencies' => $comp,
                'idCourse' => $idCourse
            ];
            $this->competenciesModel->insert($result);
        }


        $businessDomain = $this->request->getVar('namaBusinessDomain');
        $juml = -1;

        foreach ($businessDomain as $bus) {
            $juml++;
            $result2 = [
                'namaBusinessDomain' => $bus,
                'idCourse' => $idCourse
            ];
            $this->businessDomainModel->insert($result2);
        }


        $organization = $this->request->getVar('namaOrganization');
        $jumlah = -1;

        foreach ($organization as $org) {
            $jumlah++;
            $result3 = [
                'namaOrganization' => $org,
                'idCourse' => $idCourse
            ];
            $this->organizationModel->insert($result3);
        }

        return redirect()->to('/course');
    }

    public function detailCourse($idCourse)
    {
        $data = [
            'course' => $this->courseModel->getCourseDetail($idCourse)
        ];
        // dd($data);
        return view('detailCourse', $data);
    }

    public function delete($idCourse)
    {
        $this->businessDomainModel->where('idCourse', $idCourse)->delete();
        $this->competenciesModel->where('idCourse', $idCourse)->delete();
        $this->organizationModel->where('idCourse', $idCourse)->delete();
        $this->courseModel->delete($idCourse);
        return redirect()->to('/course');
    }
}
